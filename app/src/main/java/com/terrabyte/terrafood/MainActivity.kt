package com.terrabyte.terrafood

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.terrabyte.terrafood.core.Base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
