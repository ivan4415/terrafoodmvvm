package com.terrabyte.terrafood.UI.Fragments.login.data

import android.app.Application
import androidx.lifecycle.LiveData
import com.terrabyte.terrafood.IO.WebService.response.LoginResponse
import com.terrabyte.terrafood.core.Base.BaseViewModel

class LoginViewModel(application: Application) : BaseViewModel (application){

    val repository = LoginRepository()

    fun setLogin(user:String, key:String) : LiveData<LoginResponse> {
        return repository.setLogin(user,key)
    }
}