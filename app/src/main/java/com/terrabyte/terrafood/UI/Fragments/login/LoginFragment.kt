package com.terrabyte.terrafood.UI.Fragments.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.google.android.material.textfield.TextInputEditText
import com.terrabyte.terrafood.IO.WorkManagers.LoadImageFromUrlWorkManager
import com.terrabyte.terrafood.R
import com.terrabyte.terrafood.UI.Fragments.login.data.LoginViewModel
import com.terrabyte.terrafood.Utils.Constans
import com.terrabyte.terrafood.Utils.SessionManager
import com.terrabyte.terrafood.core.Base.BaseFragment

class LoginFragment : BaseFragment(),View.OnClickListener{

    lateinit var v : View
    lateinit var btnLogin : Button
    lateinit var viewModelLogin : LoginViewModel
    lateinit var etPass : TextInputEditText
    lateinit var etCorreo : TextInputEditText
    lateinit var manager : SessionManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.login_fragment, container, false)
        initView()
        viewModelLogin = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        setListeners()
        return v
    }

    override fun initView(){
        super.initView()
        btnLogin = v.findViewById(R.id.btnIniciar)
        etCorreo = v.findViewById(R.id.etCorreo)
        etPass = v.findViewById(R.id.etPass)
        manager = SessionManager(activity!!.baseContext)
    }

    override fun setListeners() {
        super.setListeners()
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btnIniciar->{
//                if(!TextUtils.isEmpty(etCorreo.text.toString())) {
//                    if(!TextUtils.isEmpty(etPass.text.toString())) {
//                        viewModelLogin.setLogin(etCorreo.text.toString(), etPass.text.toString())
                        viewModelLogin.setLogin("saqsaul@hotmail.com","tr3sco13")
                            .observe(viewLifecycleOwner, Observer {
                                val url = workDataOf(Constans.CADENA to it.data.credential.imageUri)
                                val getUriImage = OneTimeWorkRequestBuilder<LoadImageFromUrlWorkManager>()
                                    .setInputData(url)
                                    .build()
                                val bundle = bundleOf(Constans.CADENA to getUriImage.id,Constans.IS_SERIALIZABLE to it.data.credential.user)

                                navigateToFragment(R.id.action_loginFragment_to_inicioFragment,bundle)
                                WorkManager.getInstance(activity!!.applicationContext).enqueue(getUriImage)
                                manager.add(Constans.TOKEN,it.data.token)
                            })
//                    }else{
//                        Toast.makeText(activity,"Debes introducir la contraseña",Toast.LENGTH_LONG).show()
//                    }
//                }else{
//                    Toast.makeText(activity,"Debes introducir el correo",Toast.LENGTH_LONG).show()
//                }
            }
            else->{

            }
        }
    }
}