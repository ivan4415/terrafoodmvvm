package com.terrabyte.terrafood.UI.Fragments.order

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terrabyte.terrafood.IO.WebService.response.orders.TableOrderResponse

import com.terrabyte.terrafood.R
import com.terrabyte.terrafood.Utils.Constans
import com.terrabyte.terrafood.Utils.adapters.AdapterDetailOrders
import com.terrabyte.terrafood.core.Base.BaseFragment

class OrderFragment : BaseFragment() {

    companion object {
        fun newInstance() = OrderFragment()
    }

    lateinit var v : View

    private lateinit var viewModelOrder: OrderViewModel
    private lateinit var orden : TableOrderResponse
    private lateinit var rvPlatillos : RecyclerView
    private lateinit var adapter : AdapterDetailOrders

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.order_fragment, container, false)
        initView()
        return v
    }

    override fun initView() {
        rvPlatillos = v.findViewById(R.id.rvPlatillos)
        rvPlatillos.layoutManager = LinearLayoutManager(activity)
        orden = arguments!!.getSerializable(Constans.IS_SERIALIZABLE) as TableOrderResponse
        adapter = AdapterDetailOrders(activity!!,orden.orderDetailList!!)
        rvPlatillos.adapter = adapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModelOrder = ViewModelProviders.of(this).get(OrderViewModel::class.java)
        setToolBar()
    }

    private fun setToolBar() {
        setTitle(v,"Editar Orden")
        setDrawerImage(v,R.drawable.ic_arrow)
        setImageBackPressed()
    }

}
