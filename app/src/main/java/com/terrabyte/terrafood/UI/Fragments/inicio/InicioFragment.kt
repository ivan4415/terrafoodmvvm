package com.terrabyte.terrafood.UI.Fragments.inicio

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.terrabyte.terrafood.IO.Objects.User
import com.terrabyte.terrafood.IO.WebService.response.orders.OrdersResponse
import com.terrabyte.terrafood.IO.WebService.response.orders.TableOrderResponse
import com.terrabyte.terrafood.R
import com.terrabyte.terrafood.UI.FirstActivity
import com.terrabyte.terrafood.Utils.Constans
import com.terrabyte.terrafood.Utils.adapters.AdapterOrders
import com.terrabyte.terrafood.core.Base.BaseFragment
import java.util.*
import kotlin.collections.ArrayList

class InicioFragment : BaseFragment() {

    companion object {
        fun newInstance() = InicioFragment()
    }

    private lateinit var viewModelInicio: InicioViewModel
    private lateinit var imgInicio  : ImageView
    private lateinit var tvName : TextView
    private lateinit var tvLast : TextView
    private lateinit var v : View
    private lateinit var workRequest : UUID
    private lateinit var user : User
    private lateinit var adapter : AdapterOrders
    private lateinit var rvOrders : RecyclerView
    private lateinit var imgMenu : ImageView
    private lateinit var drawer : DrawerLayout
    private lateinit var order : TableOrderResponse
    private lateinit var orderResponse : OrdersResponse
    private val TAG : String = InicioFragment::class.java.simpleName

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.inicio_fragment, container, false)
        initView()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if(drawer.isDrawerOpen(GravityCompat.START)){
                drawer.closeDrawer(GravityCompat.START)
            }else{
                this.remove()
                activity!!.onBackPressed()
            }
        }
        return v
    }

    override fun initView() {
        super.initView()
        imgInicio = v.findViewById(R.id.imgInicio)
        tvName = v .findViewById(R.id.tvName)
        tvLast = v.findViewById(R.id.tvLast)
        workRequest = arguments!!.getSerializable(Constans.CADENA) as UUID
        user = arguments!!.getSerializable(Constans.IS_SERIALIZABLE) as User
        rvOrders = v.findViewById(R.id.rvOrdenes)
        imgMenu = v.findViewById(R.id.imgMenu)
        drawer = v.findViewById(R.id.drawer_layout)
        setData()
    }

    fun setData(){
        tvName.text = user.firstName
        tvLast.text = user.lastName
        setListeners()
    }

    override fun setListeners() {
        super.setListeners()
        imgMenu.setOnClickListener{
            drawer.openDrawer(GravityCompat.START)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelInicio = ViewModelProviders.of(FirstActivity.getInstance()).get(InicioViewModel::class.java)
        adapter = AdapterOrders(ArrayList(),getApplicationContext(),viewModelInicio)
        viewModelInicio.getOrders().observe(FirstActivity.getInstance(), Observer {
//            rvOrders.layoutManager = LinearLayoutManager(getApplicationContext())
//            rvOrders.adapter = adapter
            orderResponse = it
            adapter.notifyDataChange(orderResponse.data!!.orders!!)
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        WorkManager.getInstance(activity!!.applicationContext).getWorkInfoByIdLiveData(workRequest)
            .observe(viewLifecycleOwner, Observer {
                if(it!=null) {
                    when (it.state){
                        WorkInfo.State.SUCCEEDED->{
                            val stringUri = it.outputData.getString(Constans.CADENA)
                            val uri = Uri.parse(stringUri)
                            imgInicio.setImageURI(uri)

                        }
                        WorkInfo.State.FAILED -> Toast.makeText(activity,"Ups algo salio mal, no se podrá visualizar la foto de perfil.",Toast.LENGTH_SHORT).show()
                        WorkInfo.State.ENQUEUED,
                        WorkInfo.State.RUNNING,
                        WorkInfo.State.BLOCKED ,
                        WorkInfo.State.CANCELLED ->{
                            Log.v(TAG, "Servicio corriendo.")
                        }
                    }
                }
            })
//        adapter = AdapterOrders(orderResponse.data!!.orders!!,getApplicationContext(),viewModelInicio)
        rvOrders.layoutManager = LinearLayoutManager(getApplicationContext())
        rvOrders.adapter = adapter
        getItemClicked()
    }

    private fun getItemClicked(){
        viewModelInicio.getItemClicked().observe(FirstActivity.getInstance(),Observer{
            if(it != order) {
                val b = Bundle()
                order = it
                b.putSerializable(Constans.IS_SERIALIZABLE, it)
                navigateToFragment(R.id.action_inicioFragment_to_orderFragment, b)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        order = TableOrderResponse()
    }
}