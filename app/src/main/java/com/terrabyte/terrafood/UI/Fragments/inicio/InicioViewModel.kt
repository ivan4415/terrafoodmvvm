package com.terrabyte.terrafood.UI.Fragments.inicio

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.terrabyte.terrafood.IO.WebService.response.orders.OrdersResponse
import com.terrabyte.terrafood.IO.WebService.response.orders.TableOrderResponse
import org.simpleframework.xml.Order

class InicioViewModel : ViewModel() {

    val repository = InicioRepository()
    val onClicked = MutableLiveData<TableOrderResponse>()

    companion object{

        val image = MutableLiveData<Bitmap>()

        fun setImage(b : Bitmap){
            image.value = b
        }
    }

    fun getImage() : LiveData<Bitmap>{
        return image
    }

    fun getOrders() : LiveData<OrdersResponse>{
        return repository.getOrders()
    }

    fun setItemClicked(orderResponse: TableOrderResponse){
        onClicked.value = orderResponse
    }

    fun getItemClicked(): LiveData<TableOrderResponse>{
        return onClicked
    }
}
