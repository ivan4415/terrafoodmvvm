package com.terrabyte.terrafood.UI

import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.terrabyte.terrafood.R
import com.terrabyte.terrafood.core.Base.BaseActivity
import com.terrabyte.terrafood.core.Base.BaseViewModel

class FirstActivity : BaseActivity() , View.OnClickListener {

    lateinit var viewModelBase: BaseViewModel
    lateinit var drawerLayout : DrawerLayout
    lateinit var appBarConfiguration : AppBarConfiguration
    lateinit var tvOrden : TextView

    lateinit var navController: NavController
//    lateinit var navView : NavigationView

    init {
        instance = this
    }

    companion object {
        lateinit var navHost: NavController


        private lateinit var instance: FragmentActivity
        private lateinit var navigation: Navigation

        lateinit var fm: FragmentManager
        lateinit var ft: FragmentTransaction

        fun getInstance(): FragmentActivity {
            return instance
        }

        fun navigateToFragment(@IdRes id: Int) {
            if(navHost.currentDestination?.getAction(id) != null) {
                navHost.navigate(id)
            }
        }

        fun navigateToFragment(@IdRes id: Int, bundle: Bundle) {
            if(navHost.currentDestination?.getAction(id) != null) {
                navHost.navigate(id, bundle)
            }
        }

        fun navigateFromDrawer(@IdRes id : Int){
            if(navHost.currentDestination?.getAction(id)!=null){
                navHost.navigate(id)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
//        val host : NavHostFragment = supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment? ?:return
//        val navController: NavController = host.navController
        viewModelBase = ViewModelProviders.of(getInstance()).get(BaseViewModel::class.java)
        drawerLayout = findViewById(R.id.drawer_layout)
        navController = Navigation.findNavController(this,R.id.navHost)
//        navView = findViewById(R.id.nav_view)
//        tvOrden = findViewById(R.id.tvOrden)

        setUpDrawerToogle()
        navHost = findNavController(R.id.navHost)

//        NavigationUI.setupWithNavController(navView, navHost)

//        appBarConfiguration = AppBarConfiguration(navController.graph)

        fm = supportFragmentManager
        ft = fm.beginTransaction()
        setObservers()
        if (savedInstanceState == null) {
            navigateToFragment()
        }
//        tvOrden.setOnClickListener(this)
    }

    fun setUpDrawerToogle(){
        val  mDrawerToggle = object : ActionBarDrawerToggle(this,drawerLayout,R.string.nav_app_bar_open_drawer_description,R.string.nav_app_bar_navigate_up_description){}
        drawerLayout.addDrawerListener(mDrawerToggle)
        mDrawerToggle.syncState()
    }


    override fun onResume() {
        super.onResume()
    }

    private fun setObservers() {
        viewModelBase.getFragmentToChange().observe(this, Observer {
            if (it != null) {
                navigateToFragment(it)
            }
        })
        viewModelBase.getErrorProces().observe(this, Observer {
            if (it != null) {
//                TODO Implementar pantalla mamalona para representar el error.
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
        viewModelBase.getFragmentToChangeWithBundle().observe(this, Observer {
            if (it != null) {
                navigateToFragment(it.id, it.bundle)
            }
        })

    }


    override fun onClick(v: View?) {
        Toast.makeText(this,"Presionado",Toast.LENGTH_SHORT).show()
    }

    fun navigateToFragment() {
        val h = Handler()
        val r = Runnable {
            navHost.navigate(R.id.to_loginFragment)
        }
        h.postDelayed(r, 1000)
    }
}
