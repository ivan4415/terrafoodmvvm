package com.terrabyte.terrafood.UI.Fragments.inicio

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.terrabyte.terrafood.IO.WebService.response.orders.OrdersResponse
import com.terrabyte.terrafood.Utils.BaseApplication
import com.terrabyte.terrafood.core.Base.BaseRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InicioRepository : BaseRepository(){


    fun getOrders() : LiveData<OrdersResponse>{
        val liveData =MutableLiveData<OrdersResponse>()
        BaseApplication.getInstance().getControllerOrders().getOrders().enqueue(object : Callback<OrdersResponse>{
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
//                TODO error en el sistema o de conexión.

            }

            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                if(response.isSuccessful){
                    if(response.body()!!.responseCode == 200){
                        liveData.value = response.body()
                    }else{
//                        TODO error en la autorización u cualquier otro error validar
                    }
                }else{
//                    TODO error en la petición.
                }

            }

        })
        return liveData
    }
}