package com.terrabyte.terrafood.UI.Fragments.login.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.terrabyte.terrafood.IO.WebService.request.LoginRequest
import com.terrabyte.terrafood.IO.WebService.response.LoginResponse
import com.terrabyte.terrafood.Utils.BaseApplication
import com.terrabyte.terrafood.core.Base.BaseRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRepository : BaseRepository() {


    fun setLogin(user: String, key : String) : LiveData<LoginResponse>{
        val data = MutableLiveData<LoginResponse>()
        val request = LoginRequest()
        request.emal = user
        request.password = key
        BaseApplication.getInstance().getControllerLogin().setLogin(request).enqueue(object : Callback<LoginResponse>{
            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                Log.v("Login","Falló")
            }

            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if(response.isSuccessful){
                    if(response.body()!!.responseCode == 200){
                        data.value = response.body()!!
                    }else{
//                        TODO Error en la respuesta.
                        showErrorProcess("Login",response.body()!!.message,false)
                    }
                }else{
//                    TODO Error en el servicio
                    showErrorProcess("Login","Error en la autenticación.",false)
                }
            }

        })
        return data
    }
}