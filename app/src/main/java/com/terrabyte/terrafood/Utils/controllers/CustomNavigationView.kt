package com.terrabyte.terrafood.Utils.controllers

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import com.google.android.material.navigation.NavigationView
import com.google.android.material.R.attr


class CustomNavigationView(context : Context, attrs : AttributeSet?,defStyleAttr : Int ) : NavigationView(context,attrs,defStyleAttr) {

    constructor(context: Context, attrs: AttributeSet?) :this(context,attrs,attr.navigationViewStyle)

    constructor(context: Context):this(context,null)

    override fun inflateMenu(resId: Int) {
        super.inflateMenu(resId)
//        getMenuInflater
    }
}