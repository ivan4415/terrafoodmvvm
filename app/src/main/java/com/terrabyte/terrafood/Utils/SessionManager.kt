package com.terrabyte.terrafood.Utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import org.jetbrains.annotations.NotNull

class SessionManager (ctx: Context){

    private lateinit var ctx: Context
    private var currentSharedPrefrerences :String = "tfsp"
    private lateinit var pref :SharedPreferences
    private lateinit var editor : SharedPreferences.Editor

    constructor(ctx: Context,currentSharedPreferences: String):this(ctx){
        this.ctx = ctx
        this.currentSharedPrefrerences = currentSharedPrefrerences
    }

    init {
        pref = ctx.getSharedPreferences(this.currentSharedPrefrerences,Context.MODE_PRIVATE)
        editor = pref.edit()
    }

    fun removeAll(){
        editor.clear()
        editor.apply()
    }

    fun add(key : String , value : String){
        editor.putString(key,value)
        editor.apply()
    }

    fun add(key: String,value: Boolean){
        editor.putBoolean(key,value)
        editor.apply()
    }

    fun add(key: String,value: Int){
        editor.putInt(key,value)
        editor.apply()
    }

    fun add(key: String,value: Float){
        editor.putFloat(key,value)
        editor.apply()
    }

    fun getString(key: String):String{
        return pref.getString(key,"")!!
    }

    fun getBoolean(key: String):Boolean{
        return pref.getBoolean(key,false)
    }

    fun getInt(key: String):Int{
        return pref.getInt(key,0)
    }

    fun getFloat(key: String):Float{
        return pref.getFloat(key,0f)
    }

}