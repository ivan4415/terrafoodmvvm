package com.terrabyte.terrafood.Utils.adapters

import android.content.Context
import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.terrabyte.terrafood.IO.WebService.response.orders.OrderDetailListResponse
import com.terrabyte.terrafood.R
import org.w3c.dom.Text

class AdapterDetailOrders (val ctx : Context,
                           val lst : List<OrderDetailListResponse>): RecyclerView.Adapter<AdapterDetailOrders.DetailOrdersViewHolder>() {
    lateinit var v : View
    lateinit var viewHolder: DetailOrdersViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailOrdersViewHolder {
        v = LayoutInflater.from(ctx).inflate(R.layout.item_order_detail,parent,false)
        viewHolder = DetailOrdersViewHolder(v);
        return viewHolder
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    override fun onBindViewHolder(holder: DetailOrdersViewHolder, position: Int) {
        holder.onBindData(v,lst[position])
    }

    class DetailOrdersViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        lateinit var tvName : TextView
        lateinit var tvTotal : TextView
        lateinit var tvCantidad : TextView
        lateinit var imgAddController : ImageView
        lateinit var imgRemoveController : ImageView
        lateinit var tvCantidadController : TextView


        fun onBindData(itemView: View,item : OrderDetailListResponse){
            tvName = itemView.findViewById(R.id.tvName)
            tvTotal = itemView.findViewById(R.id.tvTotal)
            tvCantidad = itemView.findViewById(R.id.tvCantidad)

            tvName.text = item.dishName
            tvTotal.text = "$ ${item.subTotalAmount.toString()}"
            tvCantidad.text = item.quantity.toString()
        }

    }
}