package com.terrabyte.terrafood.Utils.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.terrabyte.terrafood.IO.WebService.response.orders.OrderDetailListResponse
import com.terrabyte.terrafood.IO.WebService.response.orders.TableOrderResponse
import com.terrabyte.terrafood.R
import com.terrabyte.terrafood.UI.Fragments.inicio.InicioViewModel

class AdapterOrders (
    var orders : List<TableOrderResponse>,
    val ctx :Context,
    val viewModel : InicioViewModel
): RecyclerView.Adapter<AdapterOrders.OrdersViewHolder>() {

    lateinit var viewHolder : OrdersViewHolder
    lateinit var v : View


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersViewHolder {
         v = LayoutInflater.from(ctx).inflate(R.layout.item_menu,parent,false)
         viewHolder = OrdersViewHolder(v);
        return viewHolder
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onBindViewHolder(holder: OrdersViewHolder, position: Int) {
        holder.onBindData(v, orders[position],ctx)
        holder.btnCobrar1.setOnClickListener{
            viewModel.setItemClicked(orders[position])
        }
        holder.btnCobrar.setOnClickListener{
            viewModel.setItemClicked(orders[position])
        }
    }

    fun notifyDataChange(orders : List<TableOrderResponse>){
        this.orders = orders
        notifyDataSetChanged()
    }

    class OrdersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var tvNumeroMesa: TextView
        lateinit var tvMonto : TextView
        lateinit var tvPlatillosOrdenados : TextView
        lateinit var btnCobrar : Button

        lateinit var rlResumen : LinearLayout
        lateinit var rlOrderDetail : RelativeLayout

        lateinit var tvNumeroMesaDetail : TextView
        lateinit var tvTotalDetail : TextView
        lateinit var tvTotalPlatillosDetail : TextView
        lateinit var tvFecha : TextView
        lateinit var btnCobrar1 : Button
        lateinit var rvListaDetail : RecyclerView
        lateinit var adapterOrders: AdapterDetailOrders


        fun onBindData(itemView : View ,tableOrderResponse: TableOrderResponse,ctx: Context){
            tvNumeroMesa = itemView.findViewById(R.id.tvNumeroMesa)
            tvNumeroMesaDetail = itemView.findViewById(R.id.tvTableDetaill)
            tvMonto = itemView.findViewById(R.id.tvMonto)
            tvTotalDetail = itemView.findViewById(R.id.tvTotalDetaill)
            tvPlatillosOrdenados = itemView.findViewById(R.id.tvPlatillosOrdenados)
            tvTotalPlatillosDetail = itemView.findViewById(R.id.tvTotalPlatillosDetail)
            btnCobrar = itemView.findViewById(R.id.btnCobrar)
            btnCobrar1 = itemView.findViewById(R.id.btnCobrarDetail)
            rlResumen = itemView.findViewById(R.id.rlResumen)
            rlOrderDetail = itemView.findViewById(R.id.rlOrderDetail)
            tvFecha = itemView.findViewById(R.id.tvFechaDetail1)
            rvListaDetail = itemView.findViewById(R.id.rvListaDetail)

            tvNumeroMesa.text = tableOrderResponse.tableNumber.toString()
            tvNumeroMesaDetail.text = tableOrderResponse.tableNumber.toString()
            tvMonto.text = tableOrderResponse.totalAmount.toString()
            tvTotalDetail.text = "$ ${tableOrderResponse.totalAmount.toString()}"

            tvPlatillosOrdenados.text = tableOrderResponse.orderDetailList!!.size.toString()
            tvTotalPlatillosDetail.text = tableOrderResponse.orderDetailList.size.toString()
            tvFecha.text = tableOrderResponse.orderDate!!.split("T")[0]
            setListeners()
            setRecycler(tableOrderResponse.orderDetailList,ctx)
        }

        fun setRecycler(lst:  List<OrderDetailListResponse>,ctx: Context){
            adapterOrders = AdapterDetailOrders(ctx,lst)
            rvListaDetail.layoutManager = LinearLayoutManager(ctx)
            rvListaDetail.adapter = adapterOrders
        }

        fun setListeners(){
            rlResumen.setOnClickListener {
                rlOrderDetail.visibility = View.VISIBLE
                rlResumen.visibility = View.GONE
            }

            rlOrderDetail.setOnClickListener{
                rlOrderDetail.visibility = View.GONE
                rlResumen.visibility = View.VISIBLE
            }

        }
    }
}