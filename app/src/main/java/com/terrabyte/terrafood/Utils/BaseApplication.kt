package com.terrabyte.terrafood.Utils

import android.app.Application
import android.content.Context
import com.facebook.stetho.Stetho
import com.terrabyte.terrafood.IO.WebService.Controllers.ControllerLogin
import com.terrabyte.terrafood.IO.WebService.Controllers.ControllerOrders

class BaseApplication : Application(){

    lateinit var ctx : Context
    private lateinit var controllerLogin : ControllerLogin
    private lateinit var controllerOrders: ControllerOrders

    companion object{
        private var instance = BaseApplication()

        fun getInstance() : BaseApplication{
            return instance
        }

    }

    override fun onCreate() {
        super.onCreate()
        initApplication()
    }

    fun initApplication(){
        instance = this
        ctx = applicationContext
        controllerLogin = ControllerLogin(ctx)
        controllerOrders = ControllerOrders(ctx)
        Stetho.initializeWithDefaults(this)
    }


    fun getControllerLogin() : ControllerLogin {
        return controllerLogin
    }

    fun getControllerOrders() : ControllerOrders{
        return controllerOrders
    }
}