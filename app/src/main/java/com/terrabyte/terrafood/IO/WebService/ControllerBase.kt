package com.terrabyte.terrafood.IO.WebService

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.terrabyte.terrafood.Utils.Utils
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit


open class ControllerBase(var context: Context){
    protected lateinit var retrofit: Retrofit

    var interceptor = HttpLoggingInterceptor()


    constructor(context : Context, url : String):this(context){
        this.context = context
        retrofit = getClientRetrofit(url)
    }

    open fun getClientHttp ():OkHttpClient.Builder{
//        val hostnameVerifier = HostnameVerifier()
        interceptor.level = (HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder()
//        httpClient.hostnameVerifier(hostnameVerifier)
        httpClient.connectTimeout(60,TimeUnit.SECONDS)
        httpClient.readTimeout(60,TimeUnit.SECONDS)
        httpClient.addInterceptor(Interceptor {
            val r= it.request().newBuilder().addHeader("Authorization","Bearer "+ Utils.getToken()).build()
            return@Interceptor it.proceed(r)
        })
        //TODO comentar en Release
        httpClient.addInterceptor(interceptor)
        httpClient.addNetworkInterceptor(StethoInterceptor())
        return httpClient
    }



    fun getClientRetrofit(url : String):Retrofit{
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(getUrlBaseFormat(url))
            .client(getClientHttp().build())
            .build()
    }

    fun getUrlBaseFormat(url : String):String{
        return if(url.endsWith("/")){
            url
        }else{
            "$url/"
        }
    }


}