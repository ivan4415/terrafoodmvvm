package com.terrabyte.terrafood.IO.WebService.services

import com.terrabyte.terrafood.IO.WebService.response.orders.OrdersResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET

interface APIOrders {

    @GET("orders")
    fun getOrders() : Call<OrdersResponse>
}