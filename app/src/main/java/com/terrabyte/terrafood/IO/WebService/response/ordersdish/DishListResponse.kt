package com.terrabyte.terrafood.IO.WebService.response.ordersdish

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class DishListResponse : Serializable {

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("name")
    val name: String? = null

    @SerializedName("price")
    val price: Int? = null

    @SerializedName("description")
    val description: Int? = null

    @SerializedName("isPackage")
    val isPackage: Boolean? = null

    override fun toString(): String {
        return "DishListResponse(id=$id, name=$name, price=$price, description=$description, isPackage=$isPackage)"
    }

}