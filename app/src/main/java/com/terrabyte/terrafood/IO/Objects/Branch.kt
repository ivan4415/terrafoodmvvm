package com.terrabyte.terrafood.IO.Objects

import com.google.gson.annotations.SerializedName

class Branch : IdName() {

    @SerializedName("brand")
    var brand : String? = null

    @SerializedName("subDomain")
    lateinit var subDomain : String

    @SerializedName("numTables")
    var numTables : Int? = null
}