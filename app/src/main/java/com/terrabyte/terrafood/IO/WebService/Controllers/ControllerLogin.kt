package com.terrabyte.terrafood.IO.WebService.Controllers

import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.terrabyte.terrafood.IO.WebService.ControllerBase
import com.terrabyte.terrafood.IO.WebService.request.LoginRequest
import com.terrabyte.terrafood.IO.WebService.response.LoginResponse
import com.terrabyte.terrafood.IO.WebService.services.APILogin
import com.terrabyte.terrafood.Utils.Utils
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import java.util.concurrent.TimeUnit

class ControllerLogin(ctx :Context,url :String = "http://54.174.147.200:8080/waiterservice/")
    : ControllerBase(ctx,url) , APILogin{

    var serviceInterface : APILogin = retrofit.create(APILogin::class.java)

    override fun setLogin(request: LoginRequest): Call<LoginResponse> {


        return serviceInterface.setLogin(request)

    }
}