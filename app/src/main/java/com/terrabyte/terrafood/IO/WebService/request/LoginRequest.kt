package com.terrabyte.terrafood.IO.WebService.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginRequest : Serializable {

    @SerializedName("email")
    lateinit var emal : String

    @SerializedName("password")
    lateinit var password :String

    override fun toString(): String {
        return "LoginRequest(emal='$emal', password='$password')"
    }
}