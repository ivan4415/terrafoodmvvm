package com.terrabyte.terrafood.IO.WebService.Controllers

import android.content.Context
import com.terrabyte.terrafood.IO.WebService.ControllerBase
import com.terrabyte.terrafood.IO.WebService.response.orders.OrdersResponse
import com.terrabyte.terrafood.IO.WebService.services.APIOrders
import retrofit2.Call

class ControllerOrders(ctx: Context,url:String= "http://54.174.147.200:8080/waiterservice/orders/") : ControllerBase(ctx,url) , APIOrders{

    val serviceInterface : APIOrders = retrofit.create(APIOrders::class.java)


    override fun getOrders(): Call<OrdersResponse> {
        return serviceInterface.getOrders()
    }

}