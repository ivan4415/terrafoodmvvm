package com.terrabyte.terrafood.IO.WebService.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class BaseResponse :Serializable{

    @SerializedName("responseCode")
    var responseCode : Int? = null

    @SerializedName("message")
    lateinit var message : String

    override fun toString(): String {
        return "BaseResponse(responseCode=$responseCode, message='$message')"
    }
}