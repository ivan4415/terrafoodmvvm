package com.terrabyte.terrafood.IO.Objects

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginData : Serializable{

    @SerializedName("token")
    lateinit var token : String

    @SerializedName("credential")
    lateinit var credential: Credential

}