package com.terrabyte.terrafood.IO.Objects

import com.google.gson.annotations.SerializedName

class UserRoles : IdName(){

    @SerializedName("role")
    lateinit var role : Role

    @SerializedName("userId")
    var userId : Int? = null
}