package com.terrabyte.terrafood.IO.WebService.response

import com.google.gson.annotations.SerializedName
import com.terrabyte.terrafood.IO.Objects.LoginData

class LoginResponse  :BaseResponse(){

    @SerializedName("data")
    lateinit var data : LoginData

}