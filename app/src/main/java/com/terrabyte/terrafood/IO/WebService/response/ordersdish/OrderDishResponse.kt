package com.terrabyte.terrafood.IO.WebService.response.ordersdish

import com.google.gson.annotations.SerializedName
import com.terrabyte.terrafood.IO.WebService.response.BaseResponse
import java.io.Serializable

open class OrderDishResponse : BaseResponse(), Serializable {

    @SerializedName("")
    val data: DataDishResponse? = null

    override fun toString(): String {
        return "OrderDishResponse(data=$data)"
    }


}