package com.terrabyte.terrafood.IO.Objects

import com.google.gson.annotations.SerializedName

class User : IdName(){

    @SerializedName("firstName")
    lateinit var firstName: String

    @SerializedName("lastName")
    lateinit var lastName : String

    @SerializedName("branch")
    lateinit var branch: Branch

    @SerializedName("waiter")
    lateinit var waiter: IdName

    @SerializedName("userRoles")
    lateinit var userRoles : ArrayList<UserRoles>
}