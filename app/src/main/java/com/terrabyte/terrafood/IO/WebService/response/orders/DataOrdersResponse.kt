package com.terrabyte.terrafood.IO.WebService.response.orders

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class DataOrdersResponse : Serializable {

    @SerializedName("orders")
    val orders: List<TableOrderResponse>? = null

    @SerializedName("numTables")
    val numTables: Int? = null

    override fun toString(): String {
        return "DataOrdersResponse(orders=$orders, numTables=$numTables)"
    }


}