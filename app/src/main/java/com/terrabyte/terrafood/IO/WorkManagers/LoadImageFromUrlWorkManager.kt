package com.terrabyte.terrafood.IO.WorkManagers

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import android.graphics.BitmapFactory
import android.content.ContextWrapper
import android.graphics.Bitmap
import androidx.work.workDataOf
import com.terrabyte.terrafood.Utils.Constans
import java.net.HttpURLConnection
import com.terrabyte.terrafood.Utils.BaseApplication
import java.io.*
import java.util.*


class LoadImageFromUrlWorkManager (appContext : Context, workerParams : WorkerParameters): Worker(appContext,workerParams) {
    lateinit var bitmap: Bitmap
    lateinit var uri : String

    override fun doWork(): Result {
        val url : String? = inputData.getString(Constans.CADENA)
        return if(loadImage(url)){
            val outputData  = workDataOf(Constans.CADENA to uri)
            Result.success(outputData)
        }else{
            Result.failure()
        }
    }

    fun loadImage(src : String?):Boolean{
        return try {
            val url = java.net.URL(src)
            val connection = url
                .openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input = connection.getInputStream()
            bitmap = BitmapFactory.decodeStream(input)
            uri = parseBitmapToUri(bitmap)
            true
        } catch (e: IOException) {
            e.printStackTrace()
            false
        }

    }

    @Throws (IOException::class)
    private fun parseBitmapToUri(bitmap: Bitmap) : String{
        val wrapper = ContextWrapper(BaseApplication.getInstance().baseContext)
        var file = wrapper.getDir("Images",Context.MODE_PRIVATE)
        file = File(file,"inicioImage${UUID.randomUUID()}.jpg")

        val stream : OutputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream)

        stream.flush()
        stream.close()

        return file.absolutePath
    }
}