package com.terrabyte.terrafood.IO.WebService.response.orders

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class TableOrderResponse : Serializable {

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("orderTypeId")
    val orderTypeId: String? = null

    @SerializedName("orderStatusId")
    val orderStatusId: String? = null

    @SerializedName("paymentMethodId")
    val paymentMethodId: String? = null

    @SerializedName("folio")
    val folio: Int? = null

    @SerializedName("tableNumber")
    val tableNumber: Int? = null

    @SerializedName("totalAmount")
    val totalAmount: Int? = null

    @SerializedName("chargeDate")
    val chargeDate: String? = null

    @SerializedName("orderDate")
    val orderDate: String? = null

    @SerializedName("orderDetailList")
    val orderDetailList: List<OrderDetailListResponse>? = null

    override fun toString(): String {
        return "TableOrderResponse(id=$id, orderTypeId=$orderTypeId, orderStatusId=$orderStatusId, paymentMethodId=$paymentMethodId, folio=$folio, tableNumber=$tableNumber, totalAmount=$totalAmount, chargeDate=$chargeDate, orderDate=$orderDate, orderDetailList=$orderDetailList)"
    }


}