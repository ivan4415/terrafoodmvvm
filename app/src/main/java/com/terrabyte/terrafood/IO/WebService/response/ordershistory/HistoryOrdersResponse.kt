package com.terrabyte.terrafood.IO.WebService.response.ordershistory

import com.google.gson.annotations.SerializedName
import com.terrabyte.terrafood.IO.WebService.response.BaseResponse
import java.io.Serializable

open class HistoryOrdersResponse : BaseResponse(), Serializable {

    @SerializedName("data")
    val data: HistoryDataResponse? = null

    override fun toString(): String {
        return "HistoryOrdersResponse(historyDataResponse=$data)"
    }

}