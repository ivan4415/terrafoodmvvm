package com.terrabyte.terrafood.IO.WebService.response.orders

import com.google.gson.annotations.SerializedName
import com.terrabyte.terrafood.IO.WebService.response.BaseResponse
import java.io.Serializable

open class OrdersResponse : BaseResponse(), Serializable {

    @SerializedName("data")
    val data: DataOrdersResponse? = null

    override fun toString(): String {
        return "OrdersResponse(data=$data)"
    }

}
