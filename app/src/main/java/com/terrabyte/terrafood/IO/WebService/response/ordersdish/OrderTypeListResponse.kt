package com.terrabyte.terrafood.IO.WebService.response.ordersdish

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class OrderTypeListResponse : Serializable {

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("description")
    val description: String? = null

    override fun toString(): String {
        return "OrderTypeListResponse(id=$id, description=$description)"
    }

}