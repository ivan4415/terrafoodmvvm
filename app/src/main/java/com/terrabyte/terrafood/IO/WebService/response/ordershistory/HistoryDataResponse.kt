package com.terrabyte.terrafood.IO.WebService.response.ordershistory

import com.google.gson.annotations.SerializedName
import com.terrabyte.terrafood.IO.WebService.response.orders.TableOrderResponse
import java.io.Serializable

open class HistoryDataResponse : Serializable {

    @SerializedName("orders")
    val orders: List<TableOrderResponse>? = null

    override fun toString(): String {
        return "HistoryDataResponse(orders=$orders)"
    }

}