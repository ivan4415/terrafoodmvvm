package com.terrabyte.terrafood.IO.WebService.response.ordersdish

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class DataDishResponse : Serializable {

    @SerializedName("dishList")
    val dishListResponse: List<DishListResponse>? = null

    @SerializedName("folio")
    val folio: Int? = null

    @SerializedName("tableNum")
    val tableNum: Int? = null

    @SerializedName("state")
    val state: String? = null

            @SerializedName("orderTypeList")
    val orderTypeList: List<OrderTypeListResponse>? = null

    override fun toString(): String {
        return "DataDishResponse(dishListResponse=$dishListResponse, folio=$folio, orderTypeList=$orderTypeList)"
    }


}