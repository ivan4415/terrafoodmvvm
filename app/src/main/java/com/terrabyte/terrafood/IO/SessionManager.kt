package com.terrabyte.terrafood.IO

import android.content.Context
import android.content.SharedPreferences
import com.terrabyte.terrafood.Utils.Constans

class SessionManager (){

    lateinit var ctx : Context
    lateinit var currentSharedPreferences: String
    lateinit var pref : SharedPreferences
    lateinit var editor : SharedPreferences.Editor



    constructor(ctx :Context):this(){
        this.ctx = ctx
        this.currentSharedPreferences = Constans.SHARED_PREFERENCES

    }

    fun addString (key : String , value : String){
        pref = ctx.getSharedPreferences(this.currentSharedPreferences,Context.MODE_PRIVATE)
        editor = pref.edit()

        editor.putString(key,value)
        editor.apply()
    }

    fun getString (key: String): String?{
        pref = ctx.getSharedPreferences(this.currentSharedPreferences,Context.MODE_PRIVATE)
        return pref.getString(key,null)
    }

}