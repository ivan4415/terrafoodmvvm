package com.terrabyte.terrafood.IO.Objects

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class IdName : Serializable{

    @SerializedName("id")
    var id : Int? = null

    @SerializedName("name")
    lateinit var name : String
}