package com.terrabyte.terrafood.IO.Objects

import com.google.gson.annotations.SerializedName

class Credential : IdName(){

    @SerializedName("user")
    lateinit var user: User

    @SerializedName("email")
    lateinit var email : String

    @SerializedName("imageUrl")
    lateinit var imageUri : String

    @SerializedName("offset")
    lateinit var offset: String

}