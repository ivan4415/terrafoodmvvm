package com.terrabyte.terrafood.IO.WebService.response.orders

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class OrderDetailListResponse : Serializable {

    @SerializedName("id")
    val id: Int? = null

    @SerializedName("orderDetailStatusId")
    val orderDetailStatusId: String? = null

    @SerializedName("promotionId")
    val promotionId: String? = null

    @SerializedName("dishId")
    val dishId: String? = null

    @SerializedName("dishName")
    val dishName: String? = null

    @SerializedName("price")
    val price: Int? = null

    @SerializedName("quantity")
    val quantity: Int? = null

    @SerializedName("subTotalAmount")
    val subTotalAmount: Int? = null

    override fun toString(): String {
        return "OrderDetailListResponse(id=$id, orderDetailStatusId=$orderDetailStatusId, promotionId=$promotionId, dishId=$dishId, dishName=$dishName, price=$price, quantity=$quantity, subTotalAmount=$subTotalAmount)"
    }


}