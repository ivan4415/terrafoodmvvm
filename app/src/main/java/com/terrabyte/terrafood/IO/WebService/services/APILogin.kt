package com.terrabyte.terrafood.IO.WebService.services

import com.terrabyte.terrafood.IO.WebService.request.LoginRequest
import com.terrabyte.terrafood.IO.WebService.response.LoginResponse
import retrofit2.http.POST
import retrofit2.Call
import retrofit2.http.Body

interface APILogin {

    @POST("signIn")
    fun setLogin(@Body request: LoginRequest): Call<LoginResponse>

}