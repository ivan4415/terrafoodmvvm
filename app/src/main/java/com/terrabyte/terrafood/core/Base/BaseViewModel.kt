package com.terrabyte.terrafood.core.Base

import android.app.Application
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.terrabyte.terrafood.IO.Objects.ErrorObject

open class BaseViewModel(application: Application) : AndroidViewModel(application) {

    class FragmentWithBundle(val id : Int, val bundle: Bundle)

    init{
        fragment.value = null
    }

    companion object{
        val valueToReturn =  MutableLiveData<Boolean>()
        val errorBundle = MutableLiveData<ErrorObject>()
        val permission = MutableLiveData<Boolean>()
        val fragment = MutableLiveData<@IdRes Int>()
        val fragmentWithBundle = MutableLiveData<FragmentWithBundle>()

        fun setShowProgress(b : Boolean){
            valueToReturn.value = b
        }

        fun showErrorProcess(b: ErrorObject){
            errorBundle.value = b
        }

        fun validatePermission(b : Boolean){
            permission.value = b
        }

        fun setFragmentToChange(@IdRes id: Int){
            this.fragment.value = id
        }

        fun setFragmentToChange(@IdRes id : Int, bundle: Bundle){
            val fragmentBundle = FragmentWithBundle(id,bundle)
            fragmentWithBundle.value = fragmentBundle
        }
    }

    fun getFragmentToChange():LiveData<Int>{
        return fragment
    }

    fun getFragmentToChangeWithBundle():LiveData<FragmentWithBundle>{
        return fragmentWithBundle
    }

    fun getErrorProces(): LiveData<ErrorObject>{
        return errorBundle
    }


}