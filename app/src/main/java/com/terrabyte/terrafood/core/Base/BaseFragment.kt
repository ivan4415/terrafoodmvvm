package com.terrabyte.terrafood.core.Base

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.terrabyte.terrafood.R
import kotlinx.android.synthetic.main.tool_bar.view.*

open class BaseFragment : Fragment(){

    lateinit var viewModel  : BaseViewModel
    lateinit var tvTitle : TextView
    lateinit var imgToolbar : ImageView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)
    }

    open fun setListeners(){

    }

    open fun initView(){

    }

    fun navigateToFragment(@IdRes id : Int){
        BaseViewModel.setFragmentToChange(id)
    }

    fun navigateToFragment(@IdRes id: Int,bundle: Bundle){
        BaseViewModel.setFragmentToChange(id,bundle)
    }

    fun getApplicationContext() : Context{
        return activity!!.applicationContext
    }

    fun setTitle(v : View, title : String){
        tvTitle = v.findViewById(R.id.tvTitle)
        tvTitle.text = title
    }

    fun setDrawerImage(v : View, @DrawableRes drawable : Int){
        imgToolbar = v.findViewById(R.id.imgMenu)
        imgToolbar.setImageDrawable(activity!!.getDrawable(drawable))
    }

    fun setImageBackPressed(){
        imgToolbar.setOnClickListener {
            activity!!.onBackPressed()
        }
    }
}