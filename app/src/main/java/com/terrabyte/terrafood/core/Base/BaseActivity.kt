package com.terrabyte.terrafood.core.Base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.terrabyte.terrafood.Utils.SessionManager

abstract class BaseActivity : AppCompatActivity() {

    lateinit var manager : SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        manager = SessionManager(this)
    }
}
