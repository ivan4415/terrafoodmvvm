package com.terrabyte.terrafood.core.Base

import androidx.lifecycle.MutableLiveData
import com.terrabyte.terrafood.IO.Objects.ErrorObject

open class BaseRepository {

    val data = MutableLiveData<Boolean>()
    val b = MutableLiveData<ErrorObject>()

    fun hideOrShowProgres(show : Boolean){
        data.value = show
        BaseViewModel.setShowProgress(show)
    }

    fun showErrorProcess(title:String, msg:String , finalizeActivity : Boolean){
        b.value = ErrorObject(title, msg, finalizeActivity)
        BaseViewModel.showErrorProcess(ErrorObject(title, msg, finalizeActivity))
    }


}